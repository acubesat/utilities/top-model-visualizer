A very simple way to plot Venn diagrams with the TOP profile of members of a subsystem.


The TOP model stands for:
* **Talent:** Talents/skills/background (filled by the member)
* **Passion:** Passions/goals (filled by the member)
* **Objective:** Objectives of the subsystem/group (filled by the coordinator)

![Example visulization](TOP_model_example.gif)

Members should ideally work on projects found in the intersection of all of the above.

The idea is based on Uri Alon's article, titled ["How to Build a Motivated Research Group"](https://www.cell.com/molecular-cell/pdf/S1097-2765(10)00040-7.pdf).

The input arguments of `TOP_model_visualizer.R` are:

* `DataFilePath`: The path to a `.csv` file with the following format:

|Person|Value|Type
|-------|--------------------|--------------------|
|Itachi Uchiha      |Microfluidics                |Talent            
|Hashirama Senju      |Software               |Passion            
|Nagato      |PCB Design               |Talent
|Zabuza Momochi      |Microscope Design               |Objective

* `Member`: The Person (Member) from the input `.csv` whose TOP profile (Talents, Passions) is to be visualized
* `Coo`: The Person (Coordinator) from the input `.csv` who states the subsystem/group Objectives

The output file is a HTML widget depicting the TOP model profile of the member (as a Venn diagram).


